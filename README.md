## base laravel 8 - jetstream livewire
- composer create
- composer require laravel/jetstream
- php artisan jetstream:install livewire
- npm install
- npm run dev
- php artisan migrate
- seed test user
- php artisan migrate:fresh --seed

## stripe invoice integration
- composer require stripe/stripe-php barryvdh/laravel-dompdf
- add stripe keys in .env
- add phone number unique to user table
- php artisan make:model Product -mcr
- php artisan make:seeder UsersTableSeeder
- php artisan make:seeder ProductsTableSeeder
- create public/images folder
- call seeder in dbseeder
- php artisan migrate:fresh --seed
- php artisan make:controller OrderController
- call method in route
- create view checkout
- create view invoice
- php artisan optimize:clear

## xero
- composer require langleyfoxall/xero-laravel
- php artisan vendor:publish --provider="LangleyFoxall\XeroLaravel\Providers\XeroLaravelServiceProvider"
- add keys in .env
- 

## invoice - laraveldaily
- composer require laraveldaily/laravel-invoices:^2.0
- php artisan invoices:install
- php artisan invoices:update
- php artisan make:controller InvoiceController



