<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use Barryvdh\DomPDF\Facade as PDF;

class OrderController extends Controller
{
    public function index() {
        $products = Product::all();
        $data = [
                'products' => $products
        ];
        return view('welcome', $data);
    }

    public function checkout ($id) {
        $product = Product::findOrFail($id);
        $user = User::find(1); // replace this with the authenticated user
        $data = [
            'product' => $product,
            'user' => $user
        ];
        $invoiceFile = "invoice-".$product->id.".pdf";
        $invoicePath = public_path("invoices/".$invoiceFile);
        PDF::loadView('invoice', $data)
            ->save($invoicePath);

        return view('checkout', $data);
    }

    public function charge(Request $request) {
        $product = Product::find($request->get('product_id'));
        try {
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

            $amount = ($product->price) * 100;
            $customer = \Stripe\Customer::create(array(
                'email' => $request->get('stripeEmail'),
                'source' => $request->get('stripeToken')
            ));
\Stripe\Charge::create(array(
                'customer' => $customer->id,
                'amount' => $amount,
                'currency' => 'usd'
            ));
            return 'Thanks for your purchase!';
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }
}
