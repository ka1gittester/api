<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Faker\Generator as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        User::create([
            'name' => 'kay',
            'email' => 'kay@l8base.com',
            'password' => bcrypt("password"),
            'phone_number' =>  $faker->e164PhoneNumber,
            'email_verified_at' => now(),
         ]);
    
    }
}
