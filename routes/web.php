<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\OrderController@index');

Route::get('/invoice', 'App\Http\Controllers\InvoiceController@index');
Route::get('/checkout/{id}', 'App\Http\Controllers\OrderController@checkout')->name('checkout');
Route::post('/charge', 'App\Http\Controllers\OrderController@charge')->name('charge');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
